package inforatge;

public class GeocodingAPI {
    public String name;
        public local_names local_names;
        public double lat;
        public double lon;
        public String country;
        public String state;

        public GeocodingAPI() {
        }

        public GeocodingAPI(String name, local_names local_names, double lat, double lon, String country, String state) {
            this.name = name;
            this.local_names = local_names;
            this.lat = lat;
            this.lon = lon;
            this.country = country;
            this.state = state;
        }
        
        public class local_names {
            //To complete
        }
}
