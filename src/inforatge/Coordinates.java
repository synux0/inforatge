package inforatge;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Coordinates {
    public double lon;
        public double lat;

        public Coordinates() {
        }

        public Coordinates(double lon, double lat) {
            this.lon = lon;
            this.lat = lat;
        }
}
