package inforatge;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Configuration {
    public double lat;
    public double lon;

    public Configuration() {
        
    }

    public Configuration(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }
}
