package inforatge;

class CurrentWeather {
    public coord coord;
    public weather[] weather;
    public String base;
    public main main;
    public int visibility;
    public wind wind;
    public clouds clouds;
    public rain rain;
    public snow snow;
    public String dt;
    public sys sys;
    public int timezone;
    public int id;
    public String name;
    public int cod;

    public CurrentWeather() {

    }

    public CurrentWeather(coord coord, weather[] weather, String base, main main,
            int visibility, wind wind, clouds clouds, rain rain, snow snow,
            String dt, sys sys, int timezone, int id, String name, int cod) {
        this.coord = coord;
        this.weather = weather;
        this.base = base;
        this.main = main;
        this.visibility = visibility;
        this.wind = wind;
        this.clouds = clouds;
        this.rain = rain;
        this.snow = snow;
        this.dt = dt;
        this.sys = sys;
        this.timezone = timezone;
        this.id = id;
        this.name = name;
        this.cod = cod;
    }

    public static class coord {
        public double lat;
        public double lon;

        public coord() {
        }
        
        public coord(double lat, double lon) {
            this.lat = lat;
            this.lon = lon;
        }
    }

    public static class weather {
        public int id;
        public String main;
        public String description;
        public String icon;
        
        public weather() {
        }
        
        public weather(int id, String main, String description, String icon) {
            this.id = id;
            this.main = main;
            this.description = description;
            this.icon = icon;
        }
    }

    public static class main {
        public double temp;
        public double feels_like;
        public double tem_min;
        public double temp_max;
        public int pressure;
        public int humidity;
        public int sea_level;
        public int grnd_level;
        
        public main() {
        }

        public main(double temp, double feels_like, double tem_min, double temp_max, int pressure, int humidity, int sea_level, int grnd_level) {
            this.temp = temp;
            this.feels_like = feels_like;
            this.tem_min = tem_min;
            this.temp_max = temp_max;
            this.pressure = pressure;
            this.humidity = humidity;
            this.sea_level = sea_level;
            this.grnd_level = grnd_level;
        }
    }

    public static class wind {
        public double speed;
        public int deg;
        public double gust;
        
        public wind() {
        }

        public wind(double speed, int deg, double gust) {
            this.speed = speed;
            this.deg = deg;
            this.gust = gust;
        }
    }

    public static class clouds {
        public int all;
        
        public clouds() {
        }

        public clouds(int all) {
            this.all = all;
        }
    }
    
    public static class rain {
        public double h1;
        public double h3;
        
        public rain() {
        }

        public rain(double h1, double h3) {
            this.h1 = h1;
            this.h3 = h3;
        }
    }

    public static class snow {
        public double h1;
        public double h3;
        
        public snow() {
        }

        public snow(double h1, double h3) {
            this.h1 = h1;
            this.h3 = h3;
        }
    }

    public static class sys {
        public int type;
        public int id;
        public int message;
        public String country;
        public int sunrise;
        public int sunset;
        
        public sys() {
        }

        public sys(int type, int id, int message, String country, int sunrise, int sunset) {
            this.type = type;
            this.id = id;
            this.message = message;
            this.country = country;
            this.sunrise = sunrise;
            this.sunset = sunset;
        }
    }
}
