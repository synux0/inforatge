package inforatge;

public class OneCallAPI {
    public double lat;
    public double lon;
    public String timezone;
    public int timezone_offset;
    public current current;
    public minutely[] minutely;
    public hourly[] hourly;
    public daily[] daily;
    public alerts[] alerts;

    public OneCallAPI() {
    }

    public OneCallAPI(double lat, double lon, String timezone, int timezone_offset, current current, minutely[] minutely, hourly[] hourly, daily[] daily, alerts[] alerts) {
        this.lat = lat;
        this.lon = lon;
        this.timezone = timezone;
        this.timezone_offset = timezone_offset;
        this.current = current;
        this.minutely = minutely;
        this.hourly = hourly;
        this.daily = daily;
        this.alerts = alerts;
    }
    
    public static class current {
        public int dt;
        public int sunrise;
        public int sunset;
        public double temp;
        public double feels_like;
        public int pressure;
        public int humidity;
        public double dew_point;
        public int clouds;
        public double uvi;
        public int visibility;
        public double wind_speed;
        public double wind_gust;
        public int wind_deg;
        public rain rain;
        public snow snow;
        public weather[] weather;

        public current() {
        }

        public current(int dt, int sunrise, int sunset, double temp, double feels_like, int pressure, int humidity, double dew_point, int clouds, double uvi, int visibility, double wind_speed, double wind_gust, int wind_deg, rain rain, snow snow, weather[] weather) {
            this.dt = dt;
            this.sunrise = sunrise;
            this.sunset = sunset;
            this.temp = temp;
            this.feels_like = feels_like;
            this.pressure = pressure;
            this.humidity = humidity;
            this.dew_point = dew_point;
            this.clouds = clouds;
            this.uvi = uvi;
            this.visibility = visibility;
            this.wind_speed = wind_speed;
            this.wind_gust = wind_gust;
            this.wind_deg = wind_deg;
            this.rain = rain;
            this.snow = snow;
            this.weather = weather;
        }
    }
    
    public static class minutely {
        public int dt;
        public double precipitation;

        public minutely() {
        }

        public minutely(int dt, double precipitation) {
            this.dt = dt;
            this.precipitation = precipitation;
        }
    }
    
    public static class hourly {
        public int dt;
        public double temp;
        public double feels_like;
        public int pressure;
        public int humidity;
        public double dew_point;
        public double uvi;
        public int clouds;
        public int visibility;
        public double wind_speed;
        public double wind_gust;
        public int wind_deg;
        public rain rain;
        public snow snow;
        public weather[] weather;

        public hourly() {
        }

        public hourly(int dt, double temp, double feels_like, int pressure, int humidity, double dew_point, double uvi, int clouds, int visibility, double wind_speed, double wind_gust, int wind_deg, rain rain, snow snow, weather[] weather) {
            this.dt = dt;
            this.temp = temp;
            this.feels_like = feels_like;
            this.pressure = pressure;
            this.humidity = humidity;
            this.dew_point = dew_point;
            this.uvi = uvi;
            this.clouds = clouds;
            this.visibility = visibility;
            this.wind_speed = wind_speed;
            this.wind_gust = wind_gust;
            this.wind_deg = wind_deg;
            this.rain = rain;
            this.snow = snow;
            this.weather = weather;
        }
    }
    
    public static class daily {
        public int dt;
        public int sunrise;
        public int sunset;
        public int moonrise;
        public int moonset;
        public double moon_phase;
        public temp temp;
        public feels_like feels_like;
        public int pressure;
        public int humidity;
        public double dew_point;
        public double wind_speed;
        public double wind_gust;
        public int wind_deg;
        public int clouds;
        public double uvi;
        public double pop;
        public double rain;
        public double snow;
        public weather[] weather;
        
        public daily() {
        }

        public daily(int dt, int sunrise, int sunset, int moonrise, int moonset, double moon_phase, temp temp, feels_like feels_like, int pressure, int humidity, double dew_point, double wind_speed, double wind_gust, int wind_deg, int clouds, double uvi, double pop, double rain, double snow, weather[] weather) {
            this.dt = dt;
            this.sunrise = sunrise;
            this.sunset = sunset;
            this.moonrise = moonrise;
            this.moonset = moonset;
            this.moon_phase = moon_phase;
            this.temp = temp;
            this.feels_like = feels_like;
            this.pressure = pressure;
            this.humidity = humidity;
            this.dew_point = dew_point;
            this.wind_speed = wind_speed;
            this.wind_gust = wind_gust;
            this.wind_deg = wind_deg;
            this.clouds = clouds;
            this.uvi = uvi;
            this.pop = pop;
            this.rain = rain;
            this.snow = snow;
            this.weather = weather;
        }
        
        public static class temp {
            public double morn;
            public double day;
            public double eve;
            public double night;
            public double min;
            public double max;

            public temp() {
            }

            public temp(double morn, double day, double eve, double night, double min, double max) {
                this.morn = morn;
                this.day = day;
                this.eve = eve;
                this.night = night;
                this.min = min;
                this.max = max;
            }
        }
        
        public static class feels_like {
            public double morn;
            public double day;
            public double eve;
            public double night;

            public feels_like() {
            }

            public feels_like(double morn, double day, double eve, double night) {
                this.morn = morn;
                this.day = day;
                this.eve = eve;
                this.night = night;
            }
        }
    }
    
    public static class alerts {
        public String sender_name;
        public String event;
        public int start;
        public int end;
        public String description;
        public tags[] tags;

        public alerts() {
        }

        public alerts(String sender_name, String event, int start, int end, String description, tags[] tags) {
            this.sender_name = sender_name;
            this.event = event;
            this.start = start;
            this.end = end;
            this.description = description;
            this.tags = tags;
        }

        public static class tags {

            public tags() {
            }
            
        }
    }
    
    //Shared classes among sections
    public static class rain {
            public double h1;

            public rain() {
            }

            public rain(double h1) {
                this.h1 = h1;
            }
        }

        public static class snow {
            public double h1;

            public snow() {
            }

            public snow(double h1) {
                this.h1 = h1;
            }
        }
    
    public static class weather {
            public int id;
            public String main;
            public String description;
            public String icon;

            public weather() {
            }

            public weather(int id, String main, String description, String icon) {
                this.id = id;
                this.main = main;
                this.description = description;
                this.icon = icon;
            }
        }
}
