package inforatge;

class CurrentAirPollution {
    public coord coord;
    public report[] list;

    public CurrentAirPollution() {
    }

    public CurrentAirPollution(coord coord, report[] list) {
        this.coord = coord;
        this.list = list;
    }
    
    
    public static class coord {
        public double lon;
        public double lat;

        public coord() {
        }
        
        public coord(double lon, double lat) {
            this.lon = lon;
            this.lat = lat;
        }
    }

    public static class report {
        public int dt;
        public main main;
        public components components;
        
        public report() {
        }

        public report(int dt, main main, components components) {
            this.dt = dt;
            this.main = main;
            this.components = components;
        }
        
        public static class main {
            public int aqi;

            public main() {
            }

            public main(int aqi) {
                this.aqi = aqi;
            }
        }
        
        public static class components {
            public double co;
            public double no;
            public double no2;
            public double o3;
            public double so2;
            public double pm2_5;
            public double pm10;
            public double nh3;
            
            public components() {
            }

            public components(double co, double no, double no2, double o3, double so2, double pm2_5, double pm10, double nh3) {
                this.co = co;
                this.no = no;
                this.no2 = no2;
                this.o3 = o3;
                this.so2 = so2;
                this.pm2_5 = pm2_5;
                this.pm10 = pm10;
                this.nh3 = nh3;
            }
        }
    }
}
