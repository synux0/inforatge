package inforatge;

public class Forecast5Day {
    public int cod;
    public int message;
    public int cnt;
    public list[] list;
    public city city;

    public Forecast5Day() {
    }

    public Forecast5Day(int cod, int message, int cnt, list[] list, city city) {
        this.cod = cod;
        this.message = message;
        this.cnt = cnt;
        this.list = list;
        this.city = city;
    }
    
    public static class list {
        public int dt;
        public main main;
        public weather[] weather;
        public clouds clouds;
        public wind wind;
        public int visibility;
        public double pop;
        public rain rain;
        public snow snow;
        public sys sys;
        public String dt_txt;

        public list() {
        }

        public list(int dt, main main, weather[] weather, clouds clouds, wind wind, int visibility, double pop, rain rain, snow snow, sys sys, String dt_txt) {
            this.dt = dt;
            this.main = main;
            this.weather = weather;
            this.clouds = clouds;
            this.wind = wind;
            this.visibility = visibility;
            this.pop = pop;
            this.rain = rain;
            this.snow = snow;
            this.sys = sys;
            this.dt_txt = dt_txt;
        }
        
        public static class main {
            public double temp;
            public double feels_like;
            public double temp_min;
            public double temp_max;
            public int pressure;
            public int sea_level;
            public int grnd_level;
            public int humidity;
            public double temp_kf;

            public main() {
            }

            public main(double temp, double feels_like, double temp_min, double temp_max, int pressure, int sea_level, int grnd_level, int humidity, double temp_kf) {
                this.temp = temp;
                this.feels_like = feels_like;
                this.temp_min = temp_min;
                this.temp_max = temp_max;
                this.pressure = pressure;
                this.sea_level = sea_level;
                this.grnd_level = grnd_level;
                this.humidity = humidity;
                this.temp_kf = temp_kf;
            }
        }
        
        public static class weather {
            public int id;
            public String main;
            public String description;
            public String icon;

            public weather() {
            }

            public weather(int id, String main, String description, String icon) {
                this.id = id;
                this.main = main;
                this.description = description;
                this.icon = icon;
            }
        }

        public static class clouds {
            public int all;

            public clouds() {
            }

            public clouds(int all) {
                this.all = all;
            }
        }

        public static class wind {
            public double speed;
            public int deg;
            public double gust;

            public wind() {
            }

            public wind(double speed, int deg, double gust) {
                this.speed = speed;
                this.deg = deg;
                this.gust = gust;
            }
        }

        

        public static class rain {
            public double h3;

            public rain() {
            }

            public rain(double h3) {
                this.h3 = h3;
            }
        }

        public static class snow {
            public double h3;

            public snow() {
            }

            public snow(double h3) {
                this.h3 = h3;
            }
        }

        public static class sys {
            public String pod;

            public sys() {
            }

            public sys(String pod) {
                this.pod = pod;
            }
        }
    }
    
    public static class city {
        public int id;
        public String name;
        public coord coord;
        public String country;
        public int population;
        public int timezone;
        public int sunrise;
        public int sunset;

        public city() {
        }

        public city(int id, String name, coord coord, String country, int population, int timezone, int sunrise, int sunset) {
            this.id = id;
            this.name = name;
            this.coord = coord;
            this.country = country;
            this.population = population;
            this.timezone = timezone;
            this.sunrise = sunrise;
            this.sunset = sunset;
        }
        
        public static class coord {
            public double lat;
            public double lon;

            public coord() {
            }

            public coord(double lat, double lon) {
                this.lat = lat;
                this.lon = lon;
            }
        }
    }
}
