package inforatge;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Inforatge {

    public static void main(String[] args) throws IOException {
        int mainOption = -1;
        String filePath = "configuration.xml";
        Configuration conf = new Configuration();
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        Gson gson = new GsonBuilder().serializeNulls().create();
        Scanner sc = new Scanner(System.in).useLocale(Locale.ENGLISH);
        
        //Initialize Configuration
        try {            
            if (!(new File(filePath)).exists()) {
                //Create default Configuration 
                conf = new Configuration();
                conf.lat = 39.467;
                conf.lon = -0.3774;
                
                //Write Configuration
                writeConfiguration(filePath, conf);
            }
            
            //Read Configuration
            conf = readConfiguration(filePath);
        
        } catch (Exception e) {
            System.out.println("(!) There has been a problem while loading or saving the configuration.");
        }
        
        //Main functionality
        do {
            try {
                mainOption = mainMenu();

                if (mainOption == 1) { //Current weather
                    String URL;
                    int locationOption = locationMenu();
                    
                    if (locationOption == 1) { //City name
                        System.out.print("City name: ");
                        String cityName = sc.nextLine();
                        System.out.print("Country code: ");
                        String countryCode = sc.nextLine();
                        System.out.println("");
                        
                        //Geocoding API call
                        String URLGeo = "https://api.openweathermap.org/geo/1.0/direct?q=" + cityName + "," + countryCode + "&appid=ff1c9558582372d80bc1230e40a0b10d";
                        
                        Request reqGeo = new Request.Builder()
                                        .url(URLGeo)
                                        .build();
                        Call call = client.newCall(reqGeo);
                        Response resGeo = call.execute();

                        String resGeoText = resGeo.body().string();
                        Type typeOfGeocodingAPI = new TypeToken<List<GeocodingAPI>>(){}.getType();
                        List<GeocodingAPI> gapi = gson.fromJson(resGeoText,typeOfGeocodingAPI);
                        
                        double lat = gapi.get(0).lat;
                        double lon = gapi.get(0).lon;

                        URL = "https://api.openweathermap.org/data/2.5/weather?lat="+ lat + "&lon=" + lon + "&units=metric&lang=en&appid=ff1c9558582372d80bc1230e40a0b10d";
                    }
                    else if (locationOption == 2) { //Coordinates
                        System.out.print("Latitude: ");
                        double lat = sc.nextDouble();
                        System.out.print("Longitude: ");
                        double lon = sc.nextDouble();
                        System.out.println("");
                        sc.nextLine();

                        URL = "https://api.openweathermap.org/data/2.5/weather?lat="+ lat + "&lon=" + lon + "&units=metric&lang=en&appid=ff1c9558582372d80bc1230e40a0b10d";
                    }
                    else { //Saved coordinates
                        double lat = conf.lat;
                        double lon = conf.lon;
                        
                        URL = "https://api.openweathermap.org/data/2.5/weather?lat="+ lat + "&lon=" + lon + "&units=metric&lang=en&appid=ff1c9558582372d80bc1230e40a0b10d";
                    }

                    Request req = new Request.Builder()
                                    .url(URL)
                                    .build();
                    Call call = client.newCall(req);
                    Response res = call.execute();

                    String resText = res.body().string().replace("1h","h1").replace("3h", "h3"); //Change incompatible variable names in received json
                    CurrentWeather cw = gson.fromJson(resText, CurrentWeather.class);
                    DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
                    otherSymbols.setDecimalSeparator('.');
                    DecimalFormat df = new DecimalFormat("#.##",otherSymbols);
                    
                    System.out.println("+-----------------------------+");
                    System.out.println("|       CURRENT WEATHER       |");
                    System.out.println("+-----------------------------+");
                    System.out.println("Location: " + cw.name + ", " + cw.sys.country);
                    System.out.println("Latitude: " + cw.coord.lat);
                    System.out.println("Longitude: " + cw.coord.lon);
                    System.out.println("");
                    System.out.println("Description: " + cw.weather[0].description);
                    System.out.println("");
                    System.out.println("Current temperature: " + cw.main.temp + "ºC");
                    System.out.println("Perceived temperature: " + cw.main.feels_like + "ºC");
                    System.out.println("");
                    System.out.println("Cloudiness: " + cw.clouds.all + "%");
                    System.out.println("");
                    if (cw.rain != null) {
                        System.out.println("Rain in 1h: " + df.format(cw.rain.h1) + "mm");
                        System.out.println("Rain in 3h: " + df.format(cw.rain.h3) + "mm");
                        System.out.println("");
                    }
                    if (cw.snow != null) {
                        System.out.println("Snow in 1h: " + df.format(cw.snow.h1) + "mm");
                        System.out.println("Snow in 3h: " + df.format(cw.snow.h3) + "mm");
                        System.out.println("");
                    }
                    System.out.println("Humidity: " + cw.main.humidity + "%");
                    System.out.println("");
                    System.out.println("Pressure: " + cw.main.pressure + "hPa");
                    System.out.println("");
                    System.out.println("Wind: " + df.format((cw.wind.speed * 3.6)) + "km/h");
                    System.out.println("");
                    System.out.println("Sunrise: " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date((long) cw.sys.sunrise * 1000)));
                    System.out.println("Sunset: " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date((long) cw.sys.sunset * 1000)));

                    screenEnd();
                }
                else if (mainOption == 2) { //Current air pollution
                    String URL;
                    int locationOption = locationMenu();
                    
                    if (locationOption == 1) { //City name
                        System.out.print("City name: ");
                        String cityName = sc.nextLine();
                        System.out.print("Country code: ");
                        String countryCode = sc.nextLine();
                        System.out.println("");
                        
                        //Geocoding API call
                        String URLGeo = "https://api.openweathermap.org/geo/1.0/direct?q=" + cityName + "," + countryCode + "&appid=ff1c9558582372d80bc1230e40a0b10d";
                        
                        Request reqGeo = new Request.Builder()
                                        .url(URLGeo)
                                        .build();
                        Call call = client.newCall(reqGeo);
                        Response resGeo = call.execute();

                        String resGeoText = resGeo.body().string();
                        Type typeOfGeocodingAPI = new TypeToken<List<GeocodingAPI>>(){}.getType();
                        List<GeocodingAPI> gapi = gson.fromJson(resGeoText,typeOfGeocodingAPI);
                        
                        double lat = gapi.get(0).lat;
                        double lon = gapi.get(0).lon;

                        URL = "https://api.openweathermap.org/data/2.5/air_pollution?lat="+ lat + "&lon=" + lon + "&units=metric&lang=en&appid=ff1c9558582372d80bc1230e40a0b10d";
                    }
                    else if (locationOption == 2) { //Coordinates
                        System.out.print("Latitude: ");
                        double lat = sc.nextDouble();
                        System.out.print("Longitude: ");
                        double lon = sc.nextDouble();
                        System.out.println("");
                        sc.nextLine();

                        URL = "https://api.openweathermap.org/data/2.5/air_pollution?lat="+ lat + "&lon=" + lon + "&units=metric&lang=en&appid=ff1c9558582372d80bc1230e40a0b10d";
                    }
                    else { //Saved coordinates
                        double lat = conf.lat;
                        double lon = conf.lon;
                        
                        URL = "https://api.openweathermap.org/data/2.5/air_pollution?lat="+ lat + "&lon=" + lon + "&units=metric&lang=en&appid=ff1c9558582372d80bc1230e40a0b10d";
                    }

                    Request req = new Request.Builder()
                                    .url(URL)
                                    .build();
                    Call call = client.newCall(req);
                    Response res = call.execute();

                    String resText = res.body().string();
                    CurrentAirPollution cap = gson.fromJson(resText, CurrentAirPollution.class);

                    String aqiDescription = "";
                    switch(cap.list[0].main.aqi) {
                        case 1:
                            aqiDescription = "(Very good)";
                            break;
                        case 2:
                            aqiDescription = "(Good)";
                            break;
                        case 3:
                            aqiDescription = "(Moderate)";
                            break;
                        case 4:
                            aqiDescription = "(Bad)";
                            break;
                        case 5:
                            aqiDescription = "(Very bad)";
                            break;
                    }

                    System.out.println("+-----------------------------------+");
                    System.out.println("|       CURRENT AIR POLLUTION       |");
                    System.out.println("+-----------------------------------+");
                    System.out.println("Latitude: " + cap.coord.lat);
                    System.out.println("Longitude: " + cap.coord.lon);
                    System.out.println("");
                    System.out.println("Air Quality Index: " + cap.list[0].main.aqi + " " + aqiDescription);
                    System.out.println("");
                    System.out.println("Components");
                    System.out.println("    CO: " + cap.list[0].components.co);
                    System.out.println("    NO: " + cap.list[0].components.no);
                    System.out.println("   NO2: " + cap.list[0].components.no2);
                    System.out.println("    O3: " + cap.list[0].components.o3);
                    System.out.println("   SO2: " + cap.list[0].components.so2);
                    System.out.println(" PM2.5: " + cap.list[0].components.pm2_5);
                    System.out.println("  PM10: " + cap.list[0].components.pm10);
                    System.out.println("   NH3: " + cap.list[0].components.nh3);
                        
                    screenEnd();
                }
                else if (mainOption == 3) { //5 day forecast
                    String URL;
                    int locationOption = locationMenu();
                    
                    if (locationOption == 1) { //City name
                        System.out.print("City name: ");
                        String cityName = sc.nextLine();
                        System.out.print("Country code: ");
                        String countryCode = sc.nextLine();
                        System.out.println("");
                        
                        //Geocoding API call
                        String URLGeo = "https://api.openweathermap.org/geo/1.0/direct?q=" + cityName + "," + countryCode + "&appid=ff1c9558582372d80bc1230e40a0b10d";
                        
                        Request reqGeo = new Request.Builder()
                                        .url(URLGeo)
                                        .build();
                        Call call = client.newCall(reqGeo);
                        Response resGeo = call.execute();

                        String resGeoText = resGeo.body().string();
                        Type typeOfGeocodingAPI = new TypeToken<List<GeocodingAPI>>(){}.getType();
                        List<GeocodingAPI> gapi = gson.fromJson(resGeoText,typeOfGeocodingAPI);
                        
                        double lat = gapi.get(0).lat;
                        double lon = gapi.get(0).lon;

                        URL = "https://api.openweathermap.org/data/2.5/forecast?lat="+ lat + "&lon=" + lon + "&units=metric&lang=en&appid=ff1c9558582372d80bc1230e40a0b10d";
                    }
                    else if (locationOption == 2) { //Coordinates
                        System.out.print("Latitude: ");
                        double lat = sc.nextDouble();
                        System.out.print("Longitude: ");
                        double lon = sc.nextDouble();
                        System.out.println("");
                        sc.nextLine();

                        URL = "https://api.openweathermap.org/data/2.5/forecast?lat="+ lat + "&lon=" + lon + "&units=metric&lang=en&appid=ff1c9558582372d80bc1230e40a0b10d";
                    }
                    else { //Saved coordinates
                        double lat = conf.lat;
                        double lon = conf.lon;
                        
                        URL = "https://api.openweathermap.org/data/2.5/forecast?lat="+ lat + "&lon=" + lon + "&units=metric&lang=en&appid=ff1c9558582372d80bc1230e40a0b10d";
                    }

                    Request req = new Request.Builder()
                                    .url(URL)
                                    .build();
                    Call call = client.newCall(req);
                    Response res = call.execute();

                    String resText = res.body().string().replace("3h","h3"); //Change incompatible variable names in received json
                    Forecast5Day f5d = gson.fromJson(resText, Forecast5Day.class);
                    DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
                    otherSymbols.setDecimalSeparator('.');
                    DecimalFormat df = new DecimalFormat("#.##",otherSymbols);
                    
                    System.out.println("+----------------------------+");
                    System.out.println("|       5 DAY FORECAST       |");
                    System.out.println("+----------------------------+");
                    System.out.println("Location: " + f5d.city.name + ", " + f5d.city.country);
                    System.out.println("Latitude: " + f5d.city.coord.lat);
                    System.out.println("Longitude: " + f5d.city.coord.lon);
                    System.out.println("");
                    System.out.println("Sunrise: " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date((long) f5d.city.sunrise * 1000)));
                    System.out.println("Sunset: " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date((long) f5d.city.sunset * 1000)));
                    System.out.println("");
                    
                    for(int i = 0; i < 40; i++) {
                        System.out.println("---------------------");
                        System.out.println("|" + f5d.list[i].dt_txt + "|");
                        System.out.println("---------------------");
                        System.out.println("Description: " + f5d.list[i].weather[0].description);
                        System.out.println("");
                        System.out.println("Temperature: " + f5d.list[0].main.temp + "ºC");
                        System.out.println("Perceived temperature: " + f5d.list[0].main.feels_like + "ºC");
                        System.out.println("");
                        System.out.println("Cloudiness: " + f5d.list[0].clouds.all);
                        System.out.println("");
                        if (f5d.list[0].rain != null) {
                            System.out.println("Rain: " + df.format(f5d.list[0].rain.h3) + "mm");
                        }
                        else {
                            System.out.println("Rain: 0mm");
                        }
                        System.out.println("");
                        if (f5d.list[0].snow != null) {
                            System.out.println("Snow: " + df.format(f5d.list[0].snow.h3) + "mm");
                        }
                        else {
                            System.out.println("Snow: 0mm");
                        }
                        System.out.println("");
                        System.out.println("Humidity: " + f5d.list[i].main.humidity + "%");
                        System.out.println("");
                        System.out.println("Pressure: " + f5d.list[i].main.pressure + "hPa");
                        System.out.println("");
                        System.out.println("Wind: " + f5d.list[i].wind.speed + "km/h");
                        System.out.println("");
                        
                    }

                    screenEnd();
                }
                else if (mainOption == 4) { //Change saved location
                    //Show current values
                    System.out.println("CURRENT LOCATION");
                    System.out.println("Latitude: " + conf.lat);
                    System.out.println("Longitude: " + conf.lon);
                    System.out.println("");
                    
                    //Ask for new values
                    System.out.println("NEW LOCATION");
                    System.out.print("Latitude: ");
                    double latitude = sc.nextDouble();
                    System.out.print("Longitude: ");
                    double longitude = sc.nextDouble();
                    System.out.println("");
                    sc.nextLine();
                    
                    //Write new configuration
                    conf.lat = latitude;
                    conf.lon = longitude;
                    writeConfiguration(filePath, conf);
                }
                else if (mainOption == 5) { //Close
                }
                else {
                    System.out.println("(!) Option not available.");
                    screenEnd();
                }
            }
            catch (Exception e) {
                System.out.println("");
                System.out.println("(!) There has been a problem.");
                screenEnd();
            }
        } while (mainOption != 5);
    }
    
    public static Configuration readConfiguration(String filePath) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Configuration.class);
        Unmarshaller unmarshaller= context.createUnmarshaller();
        Configuration conf = (Configuration) unmarshaller.unmarshal(new File(filePath));
        
        return conf;
    }
    
    public static void writeConfiguration(String filePath, Configuration conf) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Configuration.class);                
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(conf, new File(filePath));
    }
    
    public static int mainMenu() {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("+-------------------------+");
        System.out.println("|        INFORATGE        |");
        System.out.println("+-------------------------+");
        System.out.println("");
        System.out.println("MAIN MENU");
        System.out.println("1. Current weather");
        System.out.println("2. Current air pollution");
        System.out.println("3. 5 day forecast");
        System.out.println("");
        System.out.println("SETTINGS");
        System.out.println("4. Change saved location");
        System.out.println("5. Close");
        System.out.println("");
        System.out.print("Option: ");
        int selection = sc.nextInt();
        System.out.println("");
        
        return selection;
    }
    
    public static int locationMenu() {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("LOCATION");
        System.out.println("1. City name");
        System.out.println("2. Coordinates");
        System.out.println("3. Saved coordinates");
        System.out.println("");
        System.out.print("Option: ");
        int selection = sc.nextInt();
        System.out.println("");
        
        return selection;
    }
    
    public static void screenEnd() {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("");
        System.out.println("(?) Press 'enter' to return to main menu.");
        sc.nextLine();
    }
    
    //ÀàÉéÈèÍíÓóÒòÚú
}
